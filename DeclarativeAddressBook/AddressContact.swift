//
//  AddressContact.swift
//  AddressContact
//
//  Created by vikingosegundo on 25/08/2021.
//

import Foundation

enum User: Codable { case one, two }

struct Address: Identifiable, Equatable, Codable {
    enum Change {
        case add          (Add); enum Add         { case tag(name : String) }
        case remove    (Remove); enum Remove      { case tag(named: String) }
        case reassign(Reassign); enum Reassign    { case owner(to : User  ) }
        case set  (AddressData); enum AddressData { case street (to:String),
                                                         city   (to:String),
                                                         zip    (to:String),
                                                         country(to:String) }
    }
    init(owner:User,tag:String? = nil) { self.init(UUID(), owner, nil, nil, nil, nil, [tag].compactMap{$0}) }
    
    let id     : UUID
    let owner  : User
    let street : String?
    let city   : String?
    let country: String?
    let zip    : String?
    let tags   : [String]

    func alter(_ changes:Change...) -> Self { changes.reduce(self) { $0.alter($1) } }

    private func alter(_ change:Change) -> Self {
        switch change {
        case let .reassign(.owner(owner  )): return .init(id, owner, street, city, country, zip, tags                      )
        case let .set(    .street(street )): return .init(id, owner, street, city, country, zip, tags                      )
        case let .set(      .city(city   )): return .init(id, owner, street, city, country, zip, tags                      )
        case let .set(   .country(country)): return .init(id, owner, street, city, country, zip, tags                      )
        case let .set(       .zip(zip    )): return .init(id, owner, street, city, country, zip, tags                      )
        case let .add(       .tag(tag    )): return .init(id, owner, street, city, country, zip, tags + [tag]              )
        case let .remove(    .tag(tag    )): return .init(id, owner, street, city, country, zip, tags.filter({ $0 != tag }))
        }
    }
    var description: String {
          "\(tags.reduce("\n") { $0 + "[\($1)]" }) "
        + "\(street  != nil ? street!  : "") "
        + "\(zip     != nil ? zip!     : "") "
        + "\(city    != nil ? city!    : "") "
        + "\(country != nil ? country! : "") "
        + "<\(owner)>" }
    private init(_ id     : UUID,
                 _ owner  : User,
                 _ street : String?,
                 _ city   : String?,
                 _ country: String?,
                 _ zip    : String?,
                 _ tags   : [String])
    {
        self.id      = id
        self.owner   = owner
        self.street  = street
        self.city    = city
        self.country = country
        self.zip     = zip
        self.tags    = tags
    }
}

struct Contact:Identifiable, CustomStringConvertible, Codable {
    enum Change {
        case set(Set)          ; enum Set      { case family(name:String),      personal(name:String) }
        case add(Add)          ; enum Add      { case address(Address)                                }
        case update  (Update  ); enum Update   { case address(Address),         addresses([Address])  }
        case remove  (Remove  ); enum Remove   { case addresses(tagged:String), tag(named:String)     }
    }
    let id          : UUID
    let familyName  : String?
    let personalName: String?
    let addresses   : [Address]
    
    init() { self.init(UUID(),nil, nil, []) }
    func alter(_ changes:Change...) -> Self { changes.reduce(self) { $0.alter($1) } }
    var description: String {
          "\(familyName   ?? "") "
        + "\(personalName ?? "") "
        + "\((addresses.count > 0) ? addresses.reduce("") { $0 + "\($1.description)" } : "")" }
    private init(_ id:UUID, _ familyName:String?, _ personalName:String?, _ adresses:[Address]) {
        self.id           = id
        self.familyName   = familyName
        self.personalName = personalName
        self.addresses    = adresses
    }
    private func alter(_ change:Change) -> Self {
        switch change {
        case let .set   (.family   (  familyName)): return .init(id, familyName, personalName, addresses                                                             )
        case let .set   (.personal (personalName)): return .init(id, familyName, personalName, addresses                                                             )
        case let .add   (.address  (address     )): return .init(id, familyName, personalName, addresses                                                 + [address] )
        case let .update(.address  (address     )): return .init(id, familyName, personalName, addresses.filter { $0.id != address.id                  } + [address] )
        case let .update(.addresses(updated     )): return .init(id, familyName, personalName, addresses.filter { !addresses.map(\.id).contains($0.id) } + updated   )
        case let .remove(.addresses(tagged:tag  )): return .init(id, familyName, personalName, addresses.filter { !$0.tags.contains(tag)               }             )
        case let .remove(.tag      (       tag  )): return .init(id, familyName, personalName, addresses.map    { $0.alter(.remove(.tag(named:tag)))   }             )
        }
    }
}
func newContact() -> Contact { Contact() }
func new(owner:User, tagged:String? = nil) -> Address { Address(owner:owner, tag:tagged) }
