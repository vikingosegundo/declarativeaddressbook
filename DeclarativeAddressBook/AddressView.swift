//
//  AddressView.swift
//  AddressView
//
//  Created by vikingosegundo on 14/09/2021.
//

import SwiftUI

struct AddressView: View {
    init(_ a:AddressItem, user u:User?) {
        address = a
        user    = u
        street  = a.access?().street  ?? ""
        city    = a.access?().city    ?? ""
        country = a.access?().country ?? ""
    }
    var body: some View {
        if (address.access?()) != nil {
            VStack {
                TextField("Street" , text:$street )
                TextField("City"   , text:$city   )
                TextField("Country", text:$country)
                HStack {
                    if let reassign = address.reassign {
                        Button { reassign(user == .one ? .two : .one) } label: { Text("transfer")                     } }
                    if let delete = address.delete {
                        Button { delete()                             } label: { Text("delete").foregroundColor(.red) } }
                }.buttonStyle(PlainButtonStyle())
            }
            .buttonStyle(BorderlessButtonStyle())
            .onChange(of: street ){ address.change?(.set(.street (to: $0))) }
            .onChange(of: city   ){ address.change?(.set(.city   (to: $0))) }
            .onChange(of: country){ address.change?(.set(.country(to: $0))) }
        }
    }
    private let address:AddressItem
    private let user:User?
    
    @State private var street: String
    @State private var city: String
    @State private var country: String

}
