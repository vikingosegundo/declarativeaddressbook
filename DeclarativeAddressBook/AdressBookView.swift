//
//  ContentView.swift
//  DeclarativeAddressBook
//
//  Created by vikingosegundo on 25/08/2021.
//

import SwiftUI

struct AdressBookView: View {
    init(viewState vs: ViewState) { viewState = vs }
    
    @ObservedObject private var viewState    : ViewState
    
    
    @State private var updateNavigationLink: UUID? = nil
    @State private var addNavigationLink: UUID? = nil
    
    func updateBindingForItem(id: UUID) -> Binding<Bool> { .init { updateNavigationLink == id } set: { updateNavigationLink = $0 ? id : nil } }
    func addBindingForItem(   id: UUID) -> Binding<Bool> { .init { addNavigationLink    == id } set: { addNavigationLink    = $0 ? id : nil } }
    
    var body: some View {
        VStack {
            NavigationView {
                VStack {
                    List {
                        Section {
                            ForEach(viewState.contacts) { contact in
                                NavigationLink(destination:
                                                ContactView(keepDetailOpen:$updateNavigationLink, item:contact.contactItem, mode:.update),
                                               isActive: updateBindingForItem(id: contact.id)) { Text("\(contact.contactItem.intro().firstName()) \(contact.contactItem.intro().lastName())")
                                }
                            }
                        }
                        Section {
                            Button(
                                action: {
                                    switch viewState.logInOrOut {
                                    case let .in (login ): login(selectUser(from:viewState))
                                        case let .out(logout): logout() } },
                                label: {
                                    switch viewState.logInOrOut {
                                    case .in : Text("login")
                                        case .out: Text("logout").foregroundColor(.red) } } )
                            .frame(maxWidth:.infinity, alignment:.center) }
                    }
                }
                .navigationBarItems(trailing:
                                        NavigationLink(destination:
                                                        ContactView(keepDetailOpen:$addNavigationLink, item:viewState.newContactItem(from:Contact(), for:viewState.user), mode:.add)
                                                      ) { Image(systemName:viewState.addContact != nil ? "person.crop.circle.badge.plus" : "") }) }
            
        }
        .listStyle(InsetGroupedListStyle())
        .environmentObject(viewState)
    }
}
fileprivate func selectUser(from viewState:ViewState) -> User { viewState.previousUser == nil ? .one : viewState.previousUser == .one ? .two : .one }
