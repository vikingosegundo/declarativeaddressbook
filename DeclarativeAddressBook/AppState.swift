//
//  AppState.swift
//  AppState
//
//  Created by vikingosegundo on 25/08/2021.
//

import Foundation
struct AppState:Codable {
    enum Change {
        case add   (_Add)   ; enum _Add    { case contact(Contact) }
        case update(_Update); enum _Update { case contact(Contact) }
        case remove(_Remove); enum _Remove { case contact(Contact) }
        case login (User)
        case logout
    }
    
    let contacts: [Contact]
    let user:User?
    let previousUser:User?
    
    init()  { self.init(nil,nil,[]) }
    
    func alter(_ changes: [ Change ] ) -> AppState { changes.reduce(self) { $0.alter($1) } }
// MARK: -
    private init(_ user:User?,_ previousUser:User?,_ contacts:[Contact]) {
        self.user         = user
        self.previousUser = previousUser
        self.contacts     = contacts
    }
    
    private func alter (_ change:AppState.Change) -> Self {
        switch change {
        case let .login(user)              : return .init(user, previousUser, contacts                                            )
        case     .logout                   : return .init(nil ,         user, contacts                                            )
        case let .add   (.contact(contact)): return .init(user, previousUser, contacts + [contact]                                )
        case let .remove(.contact(contact)): return .init(user, previousUser, contacts.filter { $0.id != contact.id }             )
        case let .update(.contact(contact)): return .init(user, previousUser, contacts.filter { $0.id != contact.id } + [contact])
        }
    }
}
