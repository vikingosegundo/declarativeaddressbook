//
//  ContactView.swift
//  ContactView
//
//  Created by vikingosegundo on 14/09/2021.
//

import SwiftUI

struct ContactView: View {
    enum InputMode { case add; case update }
    
    @EnvironmentObject  var viewState: ViewState
    
    @Binding var keepDetailOpen: UUID?
    
    @State private var personalName: String = ""
    @State private var familyName: String = ""
    let item: ContactItem

    var body: some View {
        switch item.access {
        case .none: Text("\(item.intro().firstName()) \(item.intro().lastName())")
        case .some:
            VStack {
                List {
                    Section(header:Text("Personal")) {
                        VStack {
                            TextField("Personal Name", text:$personalName).onChange(of:personalName) { switch mode { case .add: personalName = $0 case .update:item.change?(.set(.personal(name:$0))) } }
                            TextField("Family Name"  , text:$familyName  ).onChange(of:familyName  ) { switch mode { case .add: familyName   = $0 case .update:item.change?(.set(.family  (name:$0))) } } } }
                    Section(header:HStack {
                        Text("Addresses")
                        if let add = item.add {
                            Button {
                                add(Address(owner:viewState.user!))
                            } label: { Text("add") } }}) { ForEach(viewState.addresses.filter({
                                $0.addressItem.contactId() == item.id()
                            })) { AddressView($0.addressItem, user:viewState.user) } }
                        if let delete = item.delete {
                            Section {
                                Button {
                                    delete()
                                    keepDetailOpen = nil
                                } label: { Text("delete").foregroundColor(.red) }.frame(maxWidth:.infinity, alignment:.center) } } } }
            .listStyle(InsetGroupedListStyle())
            .onAppear {
                keepDetailOpen = item.id()
                personalName = item.access?().personalName ?? ""
                familyName   = item.access?().familyName   ?? ""
            }
            .onDisappear {
                if let _ = keepDetailOpen  {
                    viewState.addContact?(item.access?().alter(.set(.family(name:familyName)), .set(.personal(name:personalName))) ?? Contact())
                }
                keepDetailOpen = nil
            }
        }
    }
    let mode: InputMode
}
