//
//  DeclarativeAddressBookApp.swift
//  DeclarativeAddressBook
//
//  Created by vikingosegundo on 25/08/2021.
//

import SwiftUI

@main
struct DeclarativeAddressBookApp: App {

    var body: some Scene { WindowGroup { AdressBookView( viewState:ViewState(store:createDiskStore()) ) } }
}
