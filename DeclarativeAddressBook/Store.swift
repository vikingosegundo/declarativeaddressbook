//
//  Store.swift
//  Store
//
//  Created by vikingosegundo on 25/08/2021.
//

import Foundation

typealias Access   = (                    ) -> AppState
typealias Change   = ( AppState.Change... ) -> ()
typealias Reset    = (                    ) -> ()
typealias Callback = ( @escaping () -> () ) -> ()
typealias Destroy  = (                    ) -> ()

typealias Store = ( state: Access, change: Change, reset: Reset, updated: Callback, destroy: Destroy )
func change(_ state: AppState, _ change: [ AppState.Change ] ) -> AppState { state.alter(change) }


func createDiskStore(
    pathInDocs   p: String      = "addressbook.json",
    fileManager fm: FileManager = .default
) -> Store
{
    var state = loadAppStateFromStore(pathInDocuments: p, fileManager: fm) { didSet { callbacks.forEach { $0() } } }
    var callbacks: [() -> ()] = []
    return (
        state   : { state },
        change  : { state     = change(state, $0); print(state); persistStore(pathInDocuments: p, state: state, fileManager: fm) },
        reset   : { state     = AppState()       ; persistStore(pathInDocuments: p, state: state, fileManager: fm) },
        updated : { callbacks = callbacks + [$0] },
        destroy : { destroyStore(pathInDocuments: p, fileManager: fm) }
    )
}

//MARK: -
private func persistStore(pathInDocuments: String, state: AppState, fileManager: FileManager ) {
    do {
        let encoder = JSONEncoder()
        #if DEBUG
        encoder.outputFormatting = .prettyPrinted
        #endif
        let data = try encoder.encode(state)
        try data.write(to: fileURL(pathInDocuments: pathInDocuments, fileManager: fileManager))
    } catch { print(error) }
}

private func loadAppStateFromStore(pathInDocuments: String, fileManager: FileManager) -> AppState {
    func new(for owner:User, tag:String? = nil) -> Address { Address(owner:owner, tag:tag) }
    do {
        return try JSONDecoder().decode(AppState.self, from: try Data(contentsOf: fileURL(pathInDocuments:pathInDocuments, fileManager:fileManager)))
    } catch { print(error) }
    return AppState().alter([.add(.contact(Contact()
                                            .alter(.set(.family  (name: "Meyer")),
                                                   .set(.personal(name: "Manuel")),
                                                   .add(.address (new(for:.one)
                                                                    .alter( .set(.city   (to: "Groningen")),
                                                                            .set(.street (to: "Bleker"   )),
                                                                            .set(.country(to: "The Netherlands")),
                                                                            .add(.tag  (name: "home"     )),
                                                                            .add(.tag  (name: "work"     ))))),
                                                   .add(.address(new(for:.two)
                                                                    .alter( .set(.city  (to: "Vechta"        )),
                                                                            .set(.street(to: "Birnbaum"      )),
                                                                            .add(.tag (name: "place of birth"))))))))])
}
private func destroyStore(pathInDocuments: String, fileManager: FileManager) {
    try? fileManager.removeItem(at: try! fileURL(pathInDocuments: pathInDocuments))
}

private func fileURL(pathInDocuments: String, fileManager: FileManager = .default) throws -> URL {
    try fileManager
        .url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        .appendingPathComponent(pathInDocuments)
}
