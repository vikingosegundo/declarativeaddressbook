//
//  ViewState.swift
//  ViewState
//
//  Created by vikingosegundo on 25/08/2021.
//

import SwiftUI

extension Collection {
    
    public func sorted<Value: Comparable>(on property: KeyPath<Element, Value>, by areInIncreasingOrder: (Value, Value) -> Bool) -> [Element] {
        sorted { currentElement, nextElement in
            areInIncreasingOrder(currentElement[keyPath: property], nextElement[keyPath: property])
        }
    }
}

//MARK: - Capabilities
typealias AddContactItem = (Contact) -> ()

enum LoginItem {
    case`in`(((User) -> ()))
    case out((()     -> ())) }

typealias AddressItem = (
    id       : (()               -> UUID   ) ,
    contactId: (()               -> UUID   ) ,
    intro    : (()               -> String ) ,
    access   : (()               -> Address)?,
    change   : ((Address.Change) -> ()     )?,
    delete   : (()               -> ()     )?,
    reassign : ((User)           -> ()     )?)

typealias NameItem = (
    firstName: () -> String,
    lastName : () -> String)

typealias ContactItem = (
    id       : (()               -> UUID            ) ,
    intro    : (()               -> NameItem        ) ,
    access   : (()               -> Contact         )?,
    change   : ((Contact.Change) -> ()              )?,
    delete   : (()               -> ()              )?,
    add      : ((Address)        -> ()              )?,
    addresses: (()               -> [AddressWrapper]) )

struct AddressWrapper: Identifiable {
    var id:UUID { addressItem.id() }
    let addressItem:AddressItem
}

struct ContactWrapper: Identifiable, Equatable {
    static func == (lhs: ContactWrapper, rhs: ContactWrapper) -> Bool {
        lhs.id == rhs.id
    }
    
    var id:UUID { contactItem.id() }
    let contactItem:ContactItem
}
//MARK: - ViewState
final
class ViewState: ObservableObject  {
    @Published var logInOrOut  : LoginItem       = .in({ _ in })
    @Published var addContact  : AddContactItem? = nil
    @Published var contacts    : [ ContactWrapper ] = []
    @Published var addresses   : [ AddressWrapper ] = []
    @Published var user        : User?           = nil
    @Published var previousUser: User?           = nil

    init(store: Store) {
        self.store = store
        self.appState = store.state()
        let exe = { self.process(store.state()) }
        store.updated { exe() }
        exe()
    }
    func newContactItem(from c: Contact, for u:User?) -> ContactItem {
        let ci = (
         id       :             id   (for:c),
         intro    :             intro(for:c),
         access   : isAuth(u) ? access(to:c) : nil,
         change   : isAuth(u) ? change(   c) : nil,
         delete   : isAuth(u) ? delete(   c) : nil,
         add      : isAuth(u) ? add   (to:c) : nil,
         addresses:             addressItems(for:c,user:u))
        return ci
    }
    private let store:Store
    private var appState: AppState = AppState()
    private func process(_ appState:AppState) {
        user         = appState.user
        previousUser = appState.previousUser
        logInOrOut   = appState.user == nil ? .in(login(user:)) : .out(logout)
        addContact   = appState.user == nil ?  nil              : add(contact:)
        contacts     = appState.contacts.map {
            ContactWrapper(contactItem: newContactItem(from:$0,for:appState.user) )
        }.sorted(by: { $0.contactItem.intro().lastName() < $1.contactItem.intro().lastName() })
        
        addresses = contacts.reduce([]) { $0 + $1.contactItem.addresses() }
    }
    private func login (user:User)                                                    {   store.change(.login(user))                                                   }
    private func logout()                                                             {   store.change(.logout     )                                                   }
    private func add   (    contact:Contact)                                          {   store.change(.update(.contact(contact)))                                        }
    private func id    (for contact:Contact) -> (                       ) -> UUID     { { contact.id                                                                 } }
    private func intro (for contact:Contact) -> (                       ) -> NameItem { {({contact.personalName ?? ""}, {contact.familyName ?? "no name"})           } }
    private func access(to  contact:Contact) -> (                       ) -> Contact  { { contact                                                                    } }
    private func delete(_   contact:Contact) -> (                       ) -> ()       { { self.store.change(.remove(.contact(contact)))                              } }
    private func change(_   contact:Contact) -> (_ change:Contact.Change) -> ()       { { self.store.change(.update(.contact(contact.alter($0))))                    } }
    private func add   (to  contact:Contact) -> (_ a     :Address       ) -> ()       {
        return { let a = $0
            self.change(contact)(.add(.address($0)));
            self.addresses = self.addresses.filter{ $0.id != a.id } + [AddressWrapper(addressItem:(
                id       :                       { a.id                                                                                                               },
                contactId:                       { contact.id                                                                                                         },
                intro    :                       { prefix(a.id)                                                                                                       },
                access   : self.isAuth (self.user) ? { a                                                                                                                  } : nil,
                change   : self.isOwner(of:$0) ? { self.store.change(.update(.contact(contact.alter(.update(.address(a.alter($0)))))))                                } : nil,
                delete   : self.isOwner(of:$0) ? { self.store.change(.update(.contact(contact.alter(.update(.addresses(contact.addresses.filter { a.id != $0.id})))))) } : nil,
                reassign : self.isOwner(of:$0) ? { self.store.change(.update(.contact(contact.alter(.update(.address(a.alter(.reassign(.owner(to:$0)))))))))          } : nil))]
        }
    }
    
    private func addressItems(for contact:Contact, user:User?) -> () -> [AddressWrapper] {{
        contact.addresses.map { a in
            AddressWrapper(addressItem:(
                id       :                      { a.id                                                                                                               },
                contactId:                      { contact.id                                                                                                         },
                intro    :                      { prefix(a.id)                                                                                                       },
                access   : self.isAuth (user) ? { a                                                                                                                  } : nil,
                change   : self.isOwner(of:a) ? { self.store.change(.update(.contact(contact.alter(.update(.address(a.alter($0)))))))                                } : nil,
                delete   : self.isOwner(of:a) ? { self.store.change(.update(.contact(contact.alter(.update(.addresses(contact.addresses.filter {$0.id != a.id})))))) } : nil,
                reassign : self.isOwner(of:a) ? { self.store.change(.update(.contact(contact.alter(.update(.address(a.alter(.reassign(.owner(to:$0)))))))))          } : nil))} }
    }
    private func isOwner(of a: Address) -> Bool { isAuth(user) && a.owner == user }
    private func isAuth(_ user:User?) -> Bool { user != nil  }

}
func prefix(_ u:UUID, maxLength m:Int = 20) -> String { String(u.uuidString.prefix(m)) }
