//
//  DeclarativeAddressBookSpecs.swift
//  DeclarativeAddressBookSpecs
//
//  Created by vikingosegundo on 14/09/2021.
//

import Quick
import Nimble
@testable import DeclarativeAddressBook

class ContactSpec: QuickSpec {
    override func spec() {
        let c = Contact().alter(.set(.personal(name: "Manuel")), .set(.family(name: "Meyer")))
        let a = Address(owner: .one).alter(.set(.street(to: "Sesame Street")),.set(.city(to: "Gotham City")))
        context("Adding an address") {
            let c = c.alter(.add(.address(a)))
            it("will be available through addresses") { expect(c.addresses)              .to(haveCount(1)) }
            it("will contain street name"           ) { expect(c.addresses.first!.street).to(equal("Sesame Street")) }
            it("will contain city name"             ) { expect(c.addresses.first!.city  ).to(equal("Gotham City")) }
            context("deleting an address") {
                let c = c.alter(.update(.addresses(c.addresses.filter { $0.id != a.id })))
                it("will be deleted from addresses") { expect(c.addresses).to(beEmpty()) }
            }
        }
    }
}
